<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Application for Group Life Insurance for Great Western Preneed Plans Trust
            to GREAT WESTERN INSURANCE COMPANY</title>
        <!--<link rel="stylesheet" type="text/css" href="/themes/tristate/elements/ea/ea.css" />-->
        <link rel="stylesheet" type="text/css" href="/earrangement/assets/css/insurance_form_1.css" />
        <style>
            div.input{
                border-bottom: 1px solid black;
                display: inline-block;
                line-height: 17px;
            }
            input:disabled, isindex:disabled, textarea:disabled {
                color: rgb(0,0,0);
            }

            .noPrint
        </style>
    </head>

    <body>
        <div class="sogs">

            <div class="sogs-content">
                <div class="sogs-app-logo"><img src="/themes/tristate/images/ea-images/sogs-app-logo.jpg"></div>
                <div class="sogs-app-name">
                    <div class="sogs-app-title">Application for Group Life Insurance for Great Western Preneed Plans Trust to GREAT WESTERN INSURANCE COMPANY</div>
                    <div class="sogs-app-title-small">3434 Washington Blvd. Ste. 100 • Ogden, Utah 84401 • (800) 621-5688</div>
                </div>

                <div class="sogs-center-side sogs-app-border">
                    <div style="width:45px; margin-left:5px;" class="input">&nbsp;</div> State Print Agent Name  <div style="width:100px;" class="input">&nbsp;</div> Agent Number <div style="width:70px;" class="input">&nbsp;</div> - <div style="width:50px;" class="input">&nbsp;</div> Date (mm/dd/yyyy) <div style="width:70px;" class="input">&nbsp;</div>
                </div>


                <div class="sogs-left-side">
                    <div class="title-app-bold">INSURED’S INFORMATION</div>
                    <div class="sogs-app-border">
                        <div class="sogs-app-text"><span style="display: inline-block;width: 130px;">Full Name</span> <?= @$user_session[$fieldprefix]['insured_name'] ?></div>
                        <div class="sogs-app-text" style="position: relative;"><span style="display: inline-block;width: 130px;">Social Security # </span> <?= @$user_session[$fieldprefix]['insured_ssn'] ?>
                            <span class="sogs-app-text-left-line" style="right: 0;width:80px;position: absolute;bottom: 0px;"> Sex <?= @$user_session[$fieldprefix]['insured_sex'] ?></span></div>
                        <div class="sogs-app-text" style="position: relative;"><span style="display: inline-block;width: 130px;">Birthdate (M/D/YYYY) </span><span><?= @$user_session[$fieldprefix]['insured_birth_month'] . '/' . @$user_session[$fieldprefix]['insured_birth_day'] . '/' . @$user_session[$fieldprefix]['insured_birth_year'] ?></span>
                            <span class="sogs-app-text-left-line" style="right: 0;width:80px;position: absolute;bottom: 0px;"> Age <?= @$user_session[$fieldprefix]['insured_age'] ?></span></div>
                        <div class="sogs-app-text-no-line">Mailing Address</div>
                        <div class="sogs-app-text"><?= @$user_session[$fieldprefix]['insured_mailing_address'] ?></div>
                        <div class="sogs-app-text">&nbsp;</div>
                        <div class="sogs-app-text">
                            <span style="display: inline-block;width: 15%;">City </span>
                            <?= @$user_session[$fieldprefix]['insured_city'] ?>
                        </div>
                        <div class="sogs-app-text" style="position: relative;">
                            <span style="display: inline-block;width: 15%;">State </span>
                            <?= @$user_session[$fieldprefix]['insured_state'] ?>
                            <span class="sogs-app-text-left-line" style="right: 0;width:30%;white-space: nowrap;overflow:hidden;position: absolute;bottom: 0px;"> Zip <?= @$user_session[$fieldprefix]['insured_zip'] ?></span>
                        </div>
                        <div class="sogs-app-text-no-line"><span style="display: inline-block;width: 20%;">Telephone #</span> <?= @$user_session[$fieldprefix]['insured_phone'] ?></div>
                    </div>

                    <div class="title-app-bold">OWNER (IF OTHER THAN INSURED)</div>
                    <div class="sogs-app-border">
                        <div class="sogs-app-text">Full Name</div>
                        <div class="sogs-app-text">Relationship</div>
                        <div class="sogs-app-text">Social Security # <span class="sogs-app-text-left-line" style="margin-left: 150px;"> Sex</span></div>
                        <div class="sogs-app-text">Address</div>

                        <div class="sogs-app-text">City, State, Zip</div>
                        <div class="sogs-app-text-no-line">Telephone #</div>
                    </div>

                    <div class="title-app-bold">PRIMARY CARE PHYSICIAN</div>
                    <div class="sogs-app-border">
                        <div class="sogs-app-text-italic">Complete only if applying for First-Day coverage.</div>
                        <div class="sogs-app-text">Name</div>
                        <div class="sogs-app-text-no-line">Address</div>
                        <div class="sogs-app-text-no-line">&nbsp;</div>
                        <div class="sogs-app-text">&nbsp;</div>
                        <div class="sogs-app-text">&nbsp;</div>
                        <div class="sogs-app-text-no-line">Telephone #</div>
                    </div>
                </div>



                <div class="sogs-right-side">

                    <div class="title-app-bold">CERTIFICATE INFORMATION</div>
                    <div class="sogs-app-border">
                        <div style="margin-top: -5px; border-bottom: 2px solid;" class="sogs-app-text-total-box">
                            <div class="sogs-app-text-total-inner"><strong><span class="total-more-padding">Total</span></strong></div>
                            <div class="sogs-app-text-total-inner">Face<br>Amount $<?= number_format($user_session['summary']['total'], 2) ?></div>
                            <div class="sogs-app-text-total-inner no-border-right">Total Paid<br>to Agent $<?= number_format($user_session['summary']['total'], 2) ?></div>
                        </div>
                        <div class="sogs-app-text-total-box">
                            <div class="sogs-app-text-total-inner"><span class="total-more-padding">Base Plan</span></div>
                            <div class="sogs-app-text-total-inner">Face<br>Amount $</div>
                            <div class="sogs-app-text-total-inner no-border-right">Modal<br>Premium $</div>
                        </div>
                        <div class="sogs-app-text-total-box">
                            <div class="sogs-app-text-total-inner">Down Payment Rider–Optional</div>
                            <div class="sogs-app-text-total-inner">Face<br>Amount $</div>
                            <div class="sogs-app-text-total-inner no-border-right">Premium<br>to Amount $</div>
                        </div>
                        <div class="sogs-app-text">Grandchild Rider <span class="sogs-app-text-small">(complete additional<br>application)</span><span class="sogs-app-text-left-line" style="margin-left: 0px;"> Premium Amt $</span></div>
                        <div class="sogs-app-text">Away-From-Home Supplement Rider <span class="sogs-app-text-left-line" style="margin-left: 6px;"> Premium Amt $</span></div>
                        <div class="sogs-app-text-total-box-dash">
                            <div class="sogs-app-text-total-inner-dash" style="padding-left:5px; padding-right:6px;">Payment<br>Method</div>
                            <div class="sogs-app-text-total-inner-dash">
                                <div>
                                    <span class="app-payment-box"><input name="" type="checkbox" checked disabled><br>Single</span>
                                                <span class="app-payment-box"><input name="" type="checkbox" value=""><br>1 yr</span>
                                                            <span class="app-payment-box"><input name="" type="checkbox" value=""><br>3 yr</span>
                                                                        <span class="app-payment-box"><input name="" type="checkbox" value=""><br>5 yr</span>
                                                                                    <span class="app-payment-box"><input name="" type="checkbox" value=""><br>10 yr</span>
                                                                                                </div>
                                                                                                </div>
                                                                                                <div class="sogs-app-text-total-inner-dash no-border-right">
                                                                                                    <div>
                                                                                                        <span class="app-payment-box"><input name="" type="checkbox" value=""><br>Mo</span>
                                                                                                                    <span class="app-payment-box"><input name="" type="checkbox" value=""><br>Qtr</span>
                                                                                                                                <span class="app-payment-box"><input name="" type="checkbox" value=""><br>Semi</span>
                                                                                                                                            <span class="app-payment-box"><input name="" type="checkbox" value=""><br>Ann</span>
                                                                                                                                                        </div>
                                                                                                                                                        </div>
                                                                                                                                                        </div>
                                                                                                                                                        <div class="sogs-app-text-total-box">
                                                                                                                                                            <div class="sogs-app-text-total-inner no-border-right" style="width:8%;"><input name="" type="checkbox" value=""></div>
                                                                                                                                                            <div class="sogs-app-text-total-inner no-border-right"  style="width: 22.33%;">Coupon<br>Sheet</div>
                                                                                                                                                            <div class="sogs-app-text-total-inner no-border-right" style="width:8%;"><input name="" type="checkbox" value=""></div>
                                                                                                                                                            <div class="sogs-app-text-total-inner no-border-right"  style="width: 35.33%;">Automatic Bank<br>Withdrawal</div>
                                                                                                                                                            <div class="sogs-app-text-total-inner no-border-right" style="width:23.33%; display: table;">
                                                                                                                                                                <input name="" type="checkbox" value="">Course<br>
                                                                                                                                                                        <input name="" type="checkbox" value="">Voyage
                                                                                                                                                                            </div>
                                                                                                                                                                            </div>
                                                                                                                                                                            <div class="sogs-app-text-no-line">Special Instructions</div>
                                                                                                                                                                            <div class="sogs-app-text-no-line">&nbsp;</div>
                                                                                                                                                                            </div>

                                                                                                                                                                            <div class="title-app-bold">BENEFICIARIES</div>
                                                                                                                                                                            <div class="sogs-app-border">
                                                                                                                                                                                <div class="sogs-app-text"><span style="display: inline-block;width: 110px;">Primary</span> <?= @$user_session[$fieldprefix]['insured_beneficiaries'] ?></div>
                                                                                                                                                                                <div class="sogs-app-text">Relationship</div>
                                                                                                                                                                                <div class="sogs-app-text">Social Security #</div>
                                                                                                                                                                                <div class="sogs-app-text-no-line">Address</div>
                                                                                                                                                                                <div class="sogs-app-text-no-line">&nbsp;</div>
                                                                                                                                                                                <div class="sogs-app-line">&nbsp;</div>
                                                                                                                                                                                <div class="sogs-app-text">Contingent</div>
                                                                                                                                                                                <div class="sogs-app-text">Relationship</div>
                                                                                                                                                                                <div class="sogs-app-text">Social Security #</div>
                                                                                                                                                                                <div class="sogs-app-text-no-line">Address</div>
                                                                                                                                                                                <div class="sogs-app-text-no-line">&nbsp;</div>
                                                                                                                                                                            </div>

                                                                                                                                                                            </div>
                                                                                                                                                                            <br style="clear:both;">

                                                                                                                                                                                <div class="sogs-center-side sogs-app-text-italic-bold">
                                                                                                                                                                                    Any person who knowingly and with intent to defraud any insurance company or other person files an application for insurance or statement of claim containing any materially false information or conceals for the purpose of misleading, information concerning any fact
                                                                                                                                                                                    material thereto commits a fraudulent insurance act, which is a crime and subjects such person to criminal and civil penalties.
                                                                                                                                                                                </div>

                                                                                                                                                                                <div class="title-app-bold">ASSIGNMENT</div>
                                                                                                                                                                                <div class="sogs-center-side sogs-app-border">
                                                                                                                                                                                    <div class="assignment-box-left">
                                                                                                                                                                                        <input type="checkbox" disabled>Yes <input type="checkbox" checked disabled>No <br>Initial Approval<br>
                                                                                                                                                                                                        <div style="width:80px; text-align: center;" class="input"><?= @$user_session[$fieldprefix]['insured_initial_1'] ?></div>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <div class="assignment-box-right">
                                                                                                                                                                                                        I hereby irrevocably <em>assign</em> and <em>transfer</em> all the benefits and proceeds of this certificate to  <div style="width:140px;" class="input">&nbsp;</div> as their interest may appear. I understand fully the effects of this assignment and transfer. It is my intention as owner to continue to pay premiums and retain ownership.
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        </div>

                                                                                                                                                                                                        <div class="sogs-center-side">
                                                                                                                                                                                                        <strong>Does the applicant have any existing policy or annuity?</strong> <input type="checkbox" checked disabled>No or <input type="checkbox" disabled>Yes
                                                                                                                                                                                                        </div>

                                                                                                                                                                                                        <div class="sogs-center-side">
                                                                                                                                                                                                        <strong>Will the proposed insurance replace any existing policy or annuity?</strong> <input type="checkbox" checked disabled>No or <input type="checkbox" disabled>Yes <span>If yes, please complete a replacement form.</span>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <div class="sogs-center-side">
                                                                                                                                                                                                        INSURED’S NAME  <div style="width:280px; padding-left: 10px;" class="input"><?= @$user_session[$fieldprefix]['insured_name2'] ?></div>
                                                                                                                                                                                                        </div>

                                                                                                                                                                                                        <div class="title-app-bold">MULTI-PAY HEALTH QUESTIONS</div>
                                                                                                                                                                                                        <div class="sogs-center-side sogs-app-border">
                                                                                                                                                                                                        <div class="multi-pay-box">
                                                                                                                                                                                                        <div class="multi-pay-box-number">1.</div>
                                                                                                                                                                                                        <div class="multi-pay-box-text">
                                                                                                                                                                                                        Now or within the last two years, has the insured been, or been told to be and refused to be, hospitalized or in a nursing facility?
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <div class="multi-pay-box-checkbox">Yes<br><input name="" type="checkbox" value=""></div>
                                                                                                                                                                                                        <div class="multi-pay-box-checkbox">No<br><input name="" type="checkbox" value=""></div>
                                                                                                                                                                                                        <div class="multi-pay-box-text-intitial">Initial<br><div style="width:40px;" class="input">&nbsp;</div></div>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <div class="multi-pay-box">
                                                                                                                                                                                                        <div class="multi-pay-box-number">2.</div>
                                                                                                                                                                                                        <div class="multi-pay-box-text">
                                                                                                                                                                                                        In the last two years, has the insured been diagnosed, treated, or prescribed drugs by a healthcare provider for any of the following diseases? Cancer, Tumor, Insulin-Dependent Diabetes, Human Immunodeficiency Virus (HIV), Acquired Immune Deficiency Syndrome (AIDS), or Acquired Immune  eficiency Syndrome-RelatedComplex (ARC), any Disorder of the Blood, Kidney, Lung, Brain, Heart, Circulatory System or Liver?
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <div class="multi-pay-box-checkbox"><input name="" type="checkbox" value=""></div>
                                                                                                                                                                                                        <div class="multi-pay-box-checkbox"><input name="" type="checkbox" value=""></div>
                                                                                                                                                                                                        <div class="multi-pay-box-text-intitial"><div style="width:40px;" class="input">&nbsp;</div></div>
                                                                                                                                                                                                        </div>

                                                                                                                                                                                                        <div class="sogs-center-side-text">I affirm that both the above health questions have been answered correctly. If either of the health questions is answered "yes," or is not answered, I will be issued a certificate with a two-year limited death benefit, per thousand dollars of face amount as outlined below:
                                                                                                                                                                                                        </div>

                                                                                                                                                                                                        <div class="plan-type-box">
                                                                                                                                                                                                        <div class="plan-type-box-inner">
                                                                                                                                                                                                        <div class="plan-type-box-col1 text-underline">Plan Type</div>
                                                                                                                                                                                                        <div class="plan-type-box-col2 text-underline">1st-Yr Monthly<br>Increases</div>
                                                                                                                                                                                                        <div class="plan-type-box-col3 text-underline">12th Month<br>Value</div>
                                                                                                                                                                                                        <div class="plan-type-box-col4 text-underline">2nd-Yr Monthly<br>Increases</div>
                                                                                                                                                                                                        <div class="plan-type-box-col5 text-underline">24th Month<br>Value</div>
                                                                                                                                                                                                        <div class="plan-type-box-col6 text-underline">25th Month Value<br>and thereafter</div>
                                                                                                                                                                                                        <div class="plan-type-box-col7 text-underline">Initials</div>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <div class="plan-type-box-inner">
                                                                                                                                                                                                        <div class="plan-type-box-col1"><input name="" type="checkbox" value=""> 1-Yr</div>
                                                                                                                                                                                                        <div class="plan-type-box-col2">$94</div>
                                                                                                                                                                                                        <div class="plan-type-box-col3">$1,000</div>
                                                                                                                                                                                                        <div class="plan-type-box-col4">-</div>
                                                                                                                                                                                                        <div class="plan-type-box-col5">$1,000</div>
                                                                                                                                                                                                        <div class="plan-type-box-col6">$1,000</div>
                                                                                                                                                                                                        <div class="plan-type-box-col7"><div style="width:40px;" class="input">&nbsp;</div></div>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <div class="plan-type-box-inner">
                                                                                                                                                                                                        <div class="plan-type-box-col1"><input name="" type="checkbox" value=""> 3-Yr</div>
                                                                                                                                                                                                        <div class="plan-type-box-col2">$41</div>
                                                                                                                                                                                                        <div class="plan-type-box-col3">$500</div>
                                                                                                                                                                                                        <div class="plan-type-box-col4">$41</div>
                                                                                                                                                                                                        <div class="plan-type-box-col5">$1,000</div>
                                                                                                                                                                                                        <div class="plan-type-box-col6">$1,000</div>
                                                                                                                                                                                                        <div class="plan-type-box-col7"><div style="width:40px;" class="input">&nbsp;</div></div>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <div class="plan-type-box-inner">
                                                                                                                                                                                                        <div class="plan-type-box-col1"><input name="" type="checkbox" value=""> 5-Yr</div>
                                                                                                                                                                                                        <div class="plan-type-box-col2">$33</div>
                                                                                                                                                                                                        <div class="plan-type-box-col3">$400</div>
                                                                                                                                                                                                        <div class="plan-type-box-col4">$41</div>
                                                                                                                                                                                                        <div class="plan-type-box-col5">$900</div>
                                                                                                                                                                                                        <div class="plan-type-box-col6">$1,000</div>
                                                                                                                                                                                                        <div class="plan-type-box-col7"><div style="width:40px;" class="input">&nbsp;</div></div>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <div class="plan-type-box-inner">
                                                                                                                                                                                                        <div class="plan-type-box-col1"><input name="" type="checkbox" value=""> 10-Yr</div>
                                                                                                                                                                                                        <div class="plan-type-box-col2">$25</div>
                                                                                                                                                                                                        <div class="plan-type-box-col3">$300</div>
                                                                                                                                                                                                        <div class="plan-type-box-col4">$33</div>
                                                                                                                                                                                                        <div class="plan-type-box-col5">$700</div>
                                                                                                                                                                                                        <div class="plan-type-box-col6">$1,000</div>
                                                                                                                                                                                                        <div class="plan-type-box-col7"><div style="width:40px;" class="input">&nbsp;</div></div>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        </div>

                                                                                                                                                                                                        </div>

                                                                                                                                                                                                        <div class="title-app-bold">AGREEMENT</div>
                                                                                                                                                                                                        <div class="sogs-center-side sogs-app-border">
                                                                                                                                                                                                        <div class="sogs-center-side-text">By signing below, I agree that: (1) To the best of my knowledge and belief, statements in this Application are complete and true. (2) When the certificate is delivered, the Insured must be alive and in the same health as described above or there will be no insurance. Also, the full premium for the chosen period must be paid by the time the certificate is delivered. (3) By accepting the certificate, I approve any change(s), correction(s), or addition(s) that Great Western made when issuing it. If my approval requires written consent, a form will be included.
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <div class="sogs-center-side-text"><strong>Insurable Interest:</strong> If the owner is other than the insured, by signing below, the owner certifies that he/she has insurable interest in the life of the insured as defined by the state statute in which the policy is issued.
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <div class="sogs-center-side-text"><strong>Authorization:</strong> By signing below, I approve of any healthcare provider, medical facility, or other person, including a Veterans Administration Hospital, giving the Great Western Insurance Company any records or information it needs about the Insured’s health. A copy of this approval will be as effective as the original. This approval is only valid for 30 months. The Insured, or a person authorized to act on behalf of the Insured, is entitled to receive a copy of this authorization upon request. <strong>I affirm that no illustration was used in the sale of this product.</strong>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <div class="agreement-box">
                                                                                                                                                                                                        <div class="agreement-box-inner">
                                                                                                                                                                                                        <div class="sogs-center-side-text">
                                                                                                                                                                                                        Signed at <div style="width:110px;" class="input"><?= @$user_session[$fieldprefix]['insured_signed_at'] ?></div>, <div style="width:120px;" class="input"><?= @$user_session[$fieldprefix]['insured_signed_date'] ?></div>
                                                                                                                                                                                                        <span class="sogs-app-text-small" style="padding-left: 80px;">City and State</span><span class="sogs-app-text-small">Month Day Year</span>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <div class="sogs-center-side-text">
                                                                                                                                                                                                        Owner <div style="width:250px;" class="input">&nbsp;</div>
                                                                                                                                                                                                        <span class="sogs-app-text-small" style="padding-left: 100px;">If Other Than Insured</span>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <div class="agreement-box-inner">
                                                                                                                                                                                                        <div class="sogs-center-side-text">
                                                                                                                                                                                                        Insured <div style="width:250px; padding-left: 10px;" class="input"><?= @$user_session[$fieldprefix]['insured_signed_insured'] ?></div>
                                                                                                                                                                                                        <span style="padding-left: 80px; width: 200px;" class="sogs-app-text-small">Parent or Guardian, If Juvenile Insured</span>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <div class="sogs-center-side-text">
                                                                                                                                                                                                        Agent <div style="width:180px;" class="input">&nbsp;</div>#<div style="width:80px;" class="input">&nbsp;</div>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <div class="sogs-center-side-text">
                                                                                                                                                                                                        Replacement of insurance is involved.  <input name="" type="checkbox" value=""> YES  <input name="" type="checkbox" value=""> NO
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <div>
                                                                                                                                                                                                        <div class="sogs-center-side-text"><strong>To the Applicant:</strong> You should hear from the Company within sixty days of the application date. If you don’t, state the facts of your application in a letter to the Secretary of Great Western Insurance Company at the address listed above.</strong>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        </div>

                                                                                                                                                                                                        <div class="title-app-bold">AUTHORIZATION AGREEMENT FOR PREAUTHORIZED AUTOMATIC BANK WITHDRAWALS</div>
                                                                                                                                                                                                        <div class="sogs-center-side sogs-app-border">
                                                                                                                                                                                                        <div class="sogs-app-text-no-line"><strong>PLEASE ATTACH A VOIDED CHECK</strong></div>
                                                                                                                                                                                                        <div class="sogs-app-text">Your Financial Institution’s Name (DEPOSITORY)</div>
                                                                                                                                                                                                        <div class="sogs-app-text">Your Financial Institution’s City and State</div>
                                                                                                                                                                                                        <div class="sogs-app-text">Your Transit (ABA) No. <span style="padding-left: 280px;">(The first nine numbers on the bottom of the check)</span></div>
                                                                                                                                                                                                        <div class="sogs-app-text">Your Account No. <span style="padding-left:180px;"><input name="" type="checkbox" value="">Checking Account <strong>or</strong> <input name="" type="checkbox" value="">Savings Account</span></div>

                                                                                                                                                                                                        <div class="sogs-center-side-text">I hereby authorize <strong>Great Western Insurance Company</strong> (THE COMPANY) to initiate debit entries. If necessary, THE COMPANY may credit entries on the above named financial institution and account.
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <div class="sogs-center-side-text">This authorization is to remain in full force and effect until THE COMPANY receives written notice of its termination. The notice must be in such time and in such manner as to allow THE COMPANY and DEPOSITORY reasonable time to act (minimum of three weeks).</strong>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <div class="sogs-app-text">Authorized Signature:</div>
                                                                                                                                                                                                        <div class="sogs-app-text">Authorized Name (please print)</div>
                                                                                                                                                                                                        <div class="sogs-app-text-no-line">Date <span class="sogs-app-text-left-line" style="margin-left: 250px; padding-bottom: 5px;"> Withdrawal Date</span></div>
                                                                                                                                                                                                        </div>

                                                                                                                                                                                                        </div>


                                                                                                                                                                                                        <center class="noPrint"><br style="clear:both;"><input type="button" value="Print" onclick="window.print();" class="noPrintVersion"><br><br></center>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        </body>
                                                                                                                                                                                                        </html>
