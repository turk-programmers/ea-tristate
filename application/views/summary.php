<?=$header?>
<form method="post" id="mainform" name="mainform" style="margin:0;">
<input type="hidden" name="scriptaction" 	id="scriptaction"	value="validate" />
<input type="hidden" name="notvalidate" 	id="notvalidate"	value="" />
<input type="hidden" name="next_form" 		id="next_form"		value="<?=$next_form?>" />
<!--Data-->

<input type="hidden" name="pkgid"	 		id="pkgid"			value="<?=$user_session['packageselected']['id']?>" />

<div id="ea" class="">
	<?php
	$this->load->view('message_badge');
	?>
	
	<div class="ea-step-title-box">
		<h1 class="ea-step-title">step 3: Order Summary</h1>
	</div>
	<div id="content-ea" >
		<div id="ea-content-inner">
	    	
	        <div class="ea-choose-options-box-inner">
	            <div class="ea-choose-options-text"><?=@$user_session['packageselected']['name']?></div>
		    	<?php
		    	if(@$user_session['is_member']){
		    		?>
		            <div class="ea-choose-options-price">$<?=number_format(@$user_session['packageselected']['member_price'],2)?></div>
		    		<?php
		    	}else{
		    		?>
		            <div class="ea-choose-options-price">$<?=number_format(@$user_session['packageselected']['price'],2)?></div>
		    		<?php
		    	}
		    	?>
	            <div class="ea-choose-options-view-details"><a href="#" onclick="toggle('platinum',this); return false;">View Details</a></div>
	        </div>
	        <div class="package-include-box" id="platinum"  style="display:none;">
	        	<?=@$user_session['packageselected']['description']?>
	        </div>
	    
	    	<?php
	    	if(is_array(@$user_session['cart'])){
	    		foreach(@$user_session['cart'] as $typekey=>$types){
	    			?>
			        <div class="merchandise-box">
			        	<div class="merchandise-title">
			        		<a href="<?=$root?>/merchandise/<?=$typekey?>">remove/change</a>
			        		<?=$merch_types[$typekey]['name']?>
			        	</div>
			            <div class="product-area">
			            	<?php
			            	foreach($types as $pid=>$item){
			            		?>
			                    <div class="product-box">
			                    	<div class="big-border">
			                            <div class="product-img"><img title="<?=$item['name']?>" src="<?=$item['thumb_url']?>" href="<?=$item['image_url']?>" /></div>
			                            <div class="product-name"><?=$item['name']?></div>
			                            <div class="product-prices"><?=number_format($item['price'],2)?></div>
			                            <div class="product-quality">QTY: <?=number_format($item['quan'])?></div>
			                    	</div>
			                    </div>
			            		<?php
			            	}
							?>
			            </div>
			        </div>
	    			<?php
	    		}
				if(is_array(@$user_session['merch_more_item_looking'])){
					?>
			        <div class="merchandise-box">
			        	<div class="merchandise-title">
			        		Item(s) you are looking for
			        	</div>
			            <div class="product-area">
			            	<ul>
				            	<?php
								foreach($user_session['merch_more_item_looking'] as $item){
									?>
									<li><?=$item?></li>
									<?php
								}
								?>
			            	</ul>
			            </div>
			        </div>
					<?php
				}
	    	}
	    	?>
	    	
	    </div>
	     
		<?php
		$this->load->view('_right_bar');
		?>
		
	</div>
</div>
<script src="<?=$cfg['root']?>/assets/js/summary.js"></script>
<!--Data-->
</form>
<?php echo $footer?>