<link rel="stylesheet" type="text/css" href="<?= $cfg['root'] ?>/assets/css/test_badge.css" />
<?php
if (@$settings['test_mode']) {
    ?>
    <div class="test_badge noAdmin">
        <div class="topleft">
            <span>
                <div><a target="_blank" href="<?= $cfg['root'] ?>/<?= @$user_session['isbam'] ? 'print_session_member' : 'print_session' ?>">Session</a></div>
            </span>
            <span>|</span>
            <span>
                <div><a target="_blank" href="debug">Debug</a></div>
            </span>
            <span>|</span>
            <span>
                <div><a href="#" onclick="propulateForm(1);
                            return false;">Populate</a></div>
                <div><a href="#" onclick="propulateForm(2);
                            return false;">Populate2</a></div>
            </span>
        </div>
        <div class="topright">
            <span>
                <div><a href="<?= $cfg['root'] ?>/<?= @$user_session['isbam'] ? 'clear_session_member' : 'clear_session' ?>">Clear Session</a></div>
                <div><a target="_blank" href="<?= $cfg['root'] ?>/clear_cache">Clear Cache</a></div>
            </span>
        </div>
        <div class="bottomleft"><?= ucfirst($pkgtypename) ?></div>
        <div class="bottomright">
            <div><a href="<?= $cfg['root'] ?>/close_test_mode">Turn-OFF Test Mode</a></div>
            <br>
            {elapsed_time} sec.
        </div>
        Test Mode
    </div>
    <?php
} elseif (is_dev()) {
    ?>
    <div class="test_badge off noAdmin">
        <a href="<?= $cfg['root'] ?>/open_test_mode" title="This can be seen by staff in bkk-office only.">Turn-On Test Mode</a>
    </div>
    <?php
}
?>
<script src="<?= $cfg['root'] ?>/assets/js/test_badge.js"></script>