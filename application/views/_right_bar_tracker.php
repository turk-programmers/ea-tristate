<?php
if (@$user_session['packageselected']['id']) {
    ?>
    <div class="ea-callbox-inner">
        <div class="ea-callbox-inner-check">&nbsp;</div>
        <div class="ea-callbox-inner-text">
            <?php
            if (@$user_session['is_member']) {
                ?>
                <div class="ea-callbox-inner-prices">$<?= number_format($user_session['packageselected']['member_price'], 2) ?></div>
                <?php
            } else {
                ?>
                <div class="ea-callbox-inner-prices">$<?= number_format($user_session['packageselected']['price'], 2) ?></div>
                <?php
            }
            ?>
            <?= $user_session['packageselected']['name'] ?>
        </div>
    </div>
    <?php
}
if (is_array(@$user_session['serviceselected'])) {
    foreach ($user_session['serviceselected'] as $s) {
        ?>
        <div class="ea-callbox-inner">
            <div class="ea-callbox-inner-check">&nbsp;</div>
            <div class="ea-callbox-inner-text">
                <div class="ea-callbox-inner-prices">$<?= number_format($s['price'], 2) ?></div>
                <?= $s['name_short'] ?>
            </div>
        </div>
        <?php
    }
}
if (is_array(@$user_session['obitonweb'])) {
    ?>
    <div class="ea-callbox-inner">
        <div class="ea-callbox-inner-check">&nbsp;</div>
        <div class="ea-callbox-inner-text">
            <div class="ea-callbox-inner-prices">$<?= number_format($user_session['obitonweb']['price'], 2) ?></div>
            <?= $user_session['obitonweb']['name'] ?>
        </div>
    </div>
    <?php
}
if (is_array(@$user_session['merch_providing_urn'])) {
    ?>
    <div class="ea-callbox-inner">
        <div class="ea-callbox-inner-check">&nbsp;</div>
        <div class="ea-callbox-inner-text">
            <div class="ea-callbox-inner-prices">$<?= number_format($user_session['merch_providing_urn']['price'], 2) ?></div>
            <?= $user_session['merch_providing_urn']['name'] ?>
        </div>
    </div>
    <?php
}
$flower_credit = @$user_session['packageselected']['flower_credit'];
$cart = @$user_session['cart'];
if (is_array($cart) and count($cart)) {
    foreach ($cart as $type => $items) {
        foreach ($items as $id => $item) {
            # Price calculate
            $price = ($item['price'] * ($item['quan'] - $item['included']));
            if ($item['type'] == 'Flower') {
                if ($price > $flower_credit) {
                    $price -= $flower_credit;
                    $flower_credit = 0;
                } else {
                    $flower_credit -= $price;
                    $price = 0;
                }
            }

            # Price formating
            $pricef = '$' . number_format($price, 2);
            if ($item['included'] and $price <= 0) {
                $pricef = 'Included';
            } elseif ($item['type'] == 'Flower' and $price <= 0) {
                $pricef = 'Credited';
            }
            ?>
            <div class="ea-callbox-inner">
                <div class="ea-callbox-inner-check">&nbsp;</div>
                <div class="ea-callbox-inner-text">
                    <div class="ea-callbox-inner-prices"><?= $pricef ?></div>
                    <?= $item['quan'] ?>x <?= $item['name'] ?>
                </div>
            </div>
            <?php
        }
    }
}
if (is_array(@$user_session['merch_more_item_looking'])) {
    foreach ($user_session['merch_more_item_looking'] as $item) {
        ?>
        <div class="ea-callbox-inner">
            <div class="ea-callbox-inner-check">&nbsp;</div>
            <div class="ea-callbox-inner-text">
                <div class="ea-callbox-inner-prices">$TBD</div>
                <?= $item ?>
            </div>
        </div>
        <?php
    }
}
if (@$user_session['payfor'] == 'full' and @ $user_session['pkgtype'] == 'preneed' and ( @$steps[$stepkey]['no'] >= @$steps['payment']['no'])) {
    ?>
    <div class="ea-callbox-inner">
        <div class="ea-callbox-inner-check">&nbsp;</div>
        <div class="ea-callbox-inner-text">
            <div class="ea-callbox-inner-prices"></div>
            Membership Fee (value is $<?= number_format($user_session['summary']['total_member'], 2) ?>)
        </div>
    </div>
    <?php
}
?>
<br>
<div class="ea-callbox-subtotal-box">
    <div class="ea-total-line">
        <div class="ea-callbox-subtotal-text">Total</div>
        <div class="ea-callbox-subtotal-prices">
            $<?= number_format($user_session['summary']['total'], 2) ?>
        </div>
    </div>
</div>