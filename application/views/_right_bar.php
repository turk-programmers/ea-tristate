<div id="ea-callbox">
    <div class="ea-callbox-box">
        <?php
        foreach ($steps as $key => $step) {
            if ($key == 'thankyou' and ! @$user_session['standalone']) {
                continue;
            }
            if ($stepkey == $key) {
                ?>
                <div class="noPrint ea-callbox-step-select"><?= $step['name'] ?></div>
                <?php
            } else {
                if ($steps[$stepkey]['no'] > $step['no']) {
                    if ($stepkey == 'thankyou') {
                        if ($key == 'payment') {
                            ?>
                            <div class="noPrint ea-callbox-step-select"><?= $step['name'] ?></div>
                            <?php
                        } else {
                            ?>
                            <div class="noPrint ea-callbox-step"><?= $step['name'] ?></div>
                            <?php
                        }
                    } else {


                        ?>
                        <div class="noPrint ea-callbox-step back_link" data-url="<?= $step['url'] ?>" title="Go back to this step."><?= $step['name'] ?></div>
                        <?php
                    }
                } else {
                    ?>
                    <div class="noPrint ea-callbox-step"><?= $step['name'] ?></div>
                    <?php
                }
            }
        }
        ?>

        <div class="noPrint ea-callbox-line">&nbsp;</div>
        <?php
        if (!@$user_session['standalone']) {
            ?>
            <div class="ea-right-tracker">
                <?php
                $this->load->view('_right_bar_tracker');
                ?>
            </div>
            <?php
        }
        ?>
        <div id="ea-bottom">
            <?php
            if (@$prev_form) {
                ?><button class="ea-bnt btn-back" title="Back" alt="Back" type="button" data-prev="<?= $prev_form ?>">BACK</button><?php
            }
            if ($stepkey == 'payment') {
                ?>
                <button class="ea-bnt btn-submit" title="Submit" alt="Submit">SUBMIT</button>
                <?php
            } elseif ($stepkey == 'thankyou') {
                ?>
                <button class="ea-bnt btn-submit" title="Home" alt="Home" type="button" onclick="location.href = '<?= $settings['home_page'] ?>';">HOME</button>
                <?php
            } elseif ($stepkey == 'authstd') {
                ?>
                <button class="ea-bnt btn-authstd-print" title="Print" alt="Print" type="button" >PRINT</button>
                <button class="ea-bnt btn-submit" title="Submit" alt="Submit">SUBMIT</button>
                <?php
            } else {
                ?>
                <button class="ea-bnt btn-submit" title="Next" alt="Next">NEXT</button>
                <?php
            }
            ?>
            <button class="ea-bnt btn-waiting" type="button" title="Waiting" alt="Waiting">WAITING</button>
        </div>
    </div>
</div>
<script src="<?= $cfg['root'] ?>/assets/js/_right_bar.js"></script>