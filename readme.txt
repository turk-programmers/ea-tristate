====================================================================================================
# 23/2/2558 Edit Status by Sippaprut
# task Form
    -  https://funeralnet.teamwork.com/tasks/3744466
    -  https://funeralnet.teamwork.com/tasks/3744395

Views SOGS
    - 'Basic services of Funeral Director and Staff' = 2145
    - TOTAL FUNERAL CHARGES = SERVICE CHARGES + MERCHANDISE + Death Certificate Copies

Controller
   sogs_v2 method
    - if user selected gold of package then price of Death Certificate Copies will equal $12
    - if user selected sliver of package then price of Death Certificate Copies will equal $6
    - if user selected Bronze of package  then price of Death Certificate Copies will equal $0

Preneed
    - + $30 membership show in the SOGS for all package

Status Ea
    ismember
        - Select from package
        - For atneed only
        - package will discount 200

    isbam
        - For become member
        - + $30 membership show in the SOGS

See more information
  - TriState Discount Package numbers.pdf

====================================================================================================