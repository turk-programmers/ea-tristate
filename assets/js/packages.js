//global variables that can be used by ALL the function son this page.
var update_select;
var page_tproc = 0;

var imgFalse;
var imgTrue;

var imgAddFalse;
var imgAddTrue;

function update_selected(){
	/*
	 * Send ajax
	 */
	var url = root+'/ajax_update_selected/';
	if(isBam) url = root+'/ajax_update_selected/';

	var servicechecked = new Array();
	$('.service_opt > div > input[type=checkbox]:checked').each(function(){
		servicechecked.push($(this).val());
	});
	wattingState();
	page_tproc = getTimeNumber();
	update_select = $.ajax({
		url:url,
		data:$.param({
			pkg:$('input#pkgid').val(),
			sids:servicechecked,
			ismember:$('.is_member_checkbox .options-check').hasClass('option-yes') ? 1 : 0,
			providingurn:$('.merch_providing_urn_checkbox .options-check').hasClass('option-yes') ? 1 : 0,
			tProc:page_tproc
			//sets:JSON.stringify(sets)
		}),
		beforeSend:function(){
                    try{
                        update_select.abort();
                    }
                    catch(err){}
		},
		success:function(res){
                    res = JSON.parse(res);
                    if(res.error_code == 'seslost'){
                        location.href = 'packages';
                    }else{
                        if(res['tProc'] < page_tproc){

                        }else{

                            // Callback process **************************************

                            $('.ea-right-tracker').html(res.tracker);
                            completeState();
                            $(window).scroll();
                        }
                    }
                    //$.pnotify(res.pnotify);
		},
		type:'POST'
	});
}
function check_pkg(pkgid, initState){
	$('.ea-choose-options-select > img').attr('src',imgFalse).parent().parent().css({
		'background-color':'#CED7CF',
		'border':'4px solid #CED7CF',
		'border-radius':'0px'
	});
	$('.ea-choose-options-select > input.pkgid').attr('checked', false);
	$('.service_opt').each(function(){
		if(!$(this).hasClass('show_in_'+pkgid)){
			$(this).hide();
			uncheck_opt($(this).data('id'));
		}
	});

	var pkg = $('.package_opt[data-pkg='+pkgid+']');
	$('.ea-choose-options-select > img', pkg).attr('src',imgTrue).parent().parent().css({
		'background-color':'#aecbd8',
		'border':'4px solid #3a3d3d',
		'border-radius':'4px'
	});
	$('.ea-choose-options-select > input.pkgid', pkg).attr('checked', true);
	$('input#pkgid').val(pkgid);
	$('.service_opt').each(function(){
		if($(this).hasClass('show_in_'+pkgid)){
			$(this).show();
		}
	});

	if(initState) return;
	update_selected();
}
function check_opt(optid){
	var opt = $('.service_opt[data-id='+optid+']');
	$('.ea-choose-additional-options-select > img', opt).attr('src', imgAddTrue);
	$('.ea-choose-additional-options-select > input.optid', opt).attr('checked', true);
}
function uncheck_opt(optid){
	var opt = $('.service_opt[data-id='+optid+']');
	$('.ea-choose-additional-options-select > img', opt).attr('src', imgAddFalse);
	$('.ea-choose-additional-options-select > input.optid', opt).attr('checked', false);
}
function toggle_opt(optid, initState){
	var opt = $('.service_opt[data-id='+optid+']');
	if($('.ea-choose-additional-options-select > input.optid', opt).attr('checked')){
		uncheck_opt(optid);
	}else{
		check_opt(optid);
	}

	if(initState) return;
	update_selected();
}
function toggle(id,obj) {
	var ele = document.getElementById(id);
	if(ele.style.display == "block") {
		ele.style.display = "none";
		obj.innerHTML = "view details";
	}
	else {
		ele.style.display = "block";
		obj.innerHTML = "less details";
	}
}
function package_togglePrice(){
	$('.package_opt').each(function(){
		var price = $(this).data('price');
		if($('.is_member_checkbox .options-check').hasClass('option-yes')){
			price = $(this).data('member-price');
		}
		$('.ea-choose-options-price', $(this)).html('$'+number_format(price,2));
	});
}

$(function(){

    $('.is_member_checkbox').click(function(){
        if($('.options-check', $(this)).hasClass('option-yes')){
            $('.options-check', $(this)).removeClass('option-yes');
            $('.options-check img', $(this)).attr('src', '/earrangement/assets/images/btn-choose-options-no.jpg');

        }else{
            $('.options-check img', $(this)).attr('src', '/earrangement/assets/images/btn-choose-options-yes.jpg');
            $('.options-check', $(this)).addClass('option-yes');
        }
        package_togglePrice();
        update_selected();
    });
    $('.merch_providing_urn_checkbox').click(function(){
        if($('.options-check', $(this)).hasClass('option-yes')){
            $('.options-check', $(this)).removeClass('option-yes');
            $('.options-check img', $(this)).attr('src', '/earrangement/assets/images/btn-choose-options-no.jpg');
        }else{
            $('.options-check img', $(this)).attr('src', '/earrangement/assets/images/btn-choose-options-yes.jpg');
            $('.options-check', $(this)).addClass('option-yes');
        }
        update_selected();
    });

    $('.select_state').change(function(){

            $('#state').val($(this).val());
            $('#county').val("");

            $('.select_county').removeClass('validate[required]').parent().hide();
            if($(this).val()){
                    $('.select_county[data-state="'+$(this).val()+'"]').val('').addClass('validate[required]').parent().css('display','inline-block');
            }else{
                    $($('.select_county').get(0)).val('').addClass('validate[required]').parent().css('display','inline-block');
            }
    });
    $('.select_county').change(function(){
            $('#county').val($(this).val());
    });

    imgFalse = $('img.img_check_no').attr('src');
    imgTrue = $('img.img_check_yes').attr('src');
    imgAddFalse = $('img.img_option_no').attr('src');
    imgAddTrue = $('img.img_option_yes').attr('src');

    $('.package_opt > .clickable').click(function(){
            var pkgid = $(this).parent().data('pkg');
            check_pkg(pkgid);
    });
    $('input.checkboximage').each(function(){
            var checkbox = $(this);
            var imgtricker = document.createElement('img');
            imgtricker.src = imgFalse;
            checkbox.after(imgtricker).hide();
    });

    $('.service_opt > .clickable').click(function(){
            var optid = $(this).parent().data('id');
            toggle_opt(optid);
    });
    $('input.checkboximage2').each(function(){
            var checkbox = $(this);
            var imgtricker = document.createElement('img');
            imgtricker.src = imgAddFalse;
            checkbox.after(imgtricker).hide();
    });

    $('#mainform').submit(function(e){
            //e.preventDefault();
            //console.log('fsdfds');
            if($('input#state').val() && $('input#county').val() && $('input#pkgid').val() == ''){
                    $('.package_alert_pointer').validationEngine('showPrompt','* Package is required.');
                    e.preventDefault();
            }
    });

});
